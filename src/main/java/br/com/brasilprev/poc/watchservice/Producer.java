package br.com.brasilprev.poc.watchservice;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;

@EnableBinding(Source.class)
public class Producer {

    private Source preConcedidoSource;

    public Producer(Source preConcedidoSource) {
        super();
        this.preConcedidoSource = preConcedidoSource;
    }

    public Source getPreConcedidoSource() {
        return this.preConcedidoSource;
    }

    public void setPreConcedidoSource(Source preConcedidoSource) {
        this.preConcedidoSource = preConcedidoSource;
    }
}
