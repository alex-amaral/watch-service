package br.com.brasilprev.poc.watchservice;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public class BeneficioDto {

    private String matricula;
    private String beneficiario;
    private String tipoRenda;
    private BigDecimal valor;
}
