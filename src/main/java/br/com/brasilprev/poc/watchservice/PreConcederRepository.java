package br.com.brasilprev.poc.watchservice;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PreConcederRepository extends CrudRepository<PreConceder,Long> {

    List<PreConceder> findByProcessado(Boolean processado);
}
