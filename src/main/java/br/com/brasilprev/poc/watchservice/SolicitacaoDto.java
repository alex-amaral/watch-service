package br.com.brasilprev.poc.watchservice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SolicitacaoDto implements Serializable {

    private Long id;
    private String matricula;
    private String beneficiario;
    private String situacao;
    private BigDecimal valor;


    public static SolicitacaoDto toSolicitacao(PreConceder preConceder){

        return new SolicitacaoDto(null,preConceder.getMatricula(), preConceder.getBeneficiario(),
                "tradicional", preConceder.getValorBeneficio());
    }
}
