package br.com.brasilprev.poc.watchservice;


import lombok.Getter;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Getter
@Entity
@Table(name = "preconceder")
public class PreConceder extends AbstractPersistable<Long> {

    private String matricula;
    private String beneficiario;
    private BigDecimal valorBeneficio;
    private String tipoRenda;
    private Boolean processado;
    private Long idBeneficio;
    private String mensagem;

    private PreConceder() {
    }

    public PreConceder processadoComSucesso(Long idBeneficio){
        this.processado = true;
        this.idBeneficio = idBeneficio;
        this.mensagem = null;
        return this;
    }

    public PreConceder processadoComErro(String mensagem){
        this.processado = true;
        this.mensagem = mensagem;
        this.idBeneficio = null;
        return this;
    }
}
