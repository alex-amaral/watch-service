package br.com.brasilprev.poc.watchservice;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@AllArgsConstructor
@RestController
public class PreSolicitacaoResource {

    private final static String URL_BENEFICIO = "http://localhost:8081/api/beneficio";

    private PreConcederRepository preConcederRepository;
    private Producer producer;
    private RestTemplate restTemplate;

    @GetMapping("processar/pre-conceder")
    public void processarPreConceder(){

        System.out.println("Inicio do processamento --->");

        preConcederRepository.findByProcessado(Boolean.FALSE).forEach(p -> {
            System.out.println("Enviado matricula --> " + p.getMatricula());
            try {
                ResponseEntity<SolicitacaoDto> result = restTemplate
                        .postForEntity(URL_BENEFICIO,  SolicitacaoDto.toSolicitacao(p), SolicitacaoDto.class);
                    p.processadoComSucesso(result.getBody().getId());
            } catch ( HttpClientErrorException e){
                p.processadoComErro(e.getMessage());
            }
            preConcederRepository.save(p);
        });
        System.out.println("Fim do processamento --->");
    }

   private void processarMensagem( PreConceder preConceder){
        producer.getPreConcedidoSource()
                .output()
                .send(MessageBuilder.withPayload(preConceder)
                        .setHeader("type", "SOLICITACAO")
                        .build());
    }
}
